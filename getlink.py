import urllib.request as ur
import json


def get_all_video_in_channel(channel_id):
    api_key = "AIzaSyCalCePCTmuD_7hwsLZGVbcAlHzTN3AqVc"

    base_video_url = 'https://www.youtube.com/watch?v='
    base_search_url = 'https://www.googleapis.com/youtube/v3/search?'

    first_url = base_search_url + 'key={}&channelId={}&part=snippet,id&order=date&maxResults=25'.format(api_key, channel_id)

    video_links = []
    url = first_url
    while 1:
        inp = ur.urlopen(url)
        resp = json.load(inp)

        for i in resp['items']:
            if i['id']['kind'] == "youtube#video":
                video_links.append(base_video_url + i['id']['videoId'])
        try:
            next_page_token = resp['nextPageToken']
            url = first_url + '&pageToken={}'.format(next_page_token)
        except:
            break
            # for i in video_links:
            #     print(i)

    return video_links

if __name__ == '__main__':

    for link in get_all_video_in_channel("UC4teNNTOky4HAEh4S0q4Ahg"):
        print(link)


